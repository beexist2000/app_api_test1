import 'package:app_api_test1/components/component_count_title.dart';
import 'package:app_api_test1/components/component_list_item.dart';
import 'package:app_api_test1/components/component_no_contents.dart';
import 'package:app_api_test1/model/hospital_list_item.dart';
import 'package:app_api_test1/repository/repo_hospital_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _scrollController = ScrollController();

  final _formKey = GlobalKey<FormBuilderState>();
  var areaOptions = ['전체', '경기도 안산시', '경기도 수원시'];
  bool _areaHasError = false;

  List<HospitalListItem> _list = [];
  int _page = 1; // 현재 페이지
  int _totalPage = 1; // 총 페이지 개수
  int _perPage = 10; // 한 페이지 당 보여줄 아이템 개수
  int _totalCount = 0; // 총 아이템 개수
  int _currentCount = 0; // 현재 보여지고 있는 아이템의 번호
  int _matchCount = 0; // 검색 결과 개수
  String _searchArea = '전체';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  Future<void> _loadItems({bool refresh = false}) async {
    if (refresh) {
      _list = [];
      _page = 1;
      _totalPage = 1;
      _perPage = 10;
      _currentCount = 0;
      _matchCount = 0;
    }


    if(_page <= _totalPage) {
      await RepoHospitalList()
          .getList(page: _page, perPage: _perPage, searchArea: _searchArea)
          .then((res) => {
        setState(() {
          _totalPage = (res.matchCount/res.perPage).ceil();
          _totalCount = res.totalCount;
          _currentCount = res.currentCount;
          _matchCount = res.matchCount;

          _list = [..._list, ...?res.data];

          _page++;
        })
      })
          .catchError((err) => {
        debugPrint(err)
      });
    }

    if (refresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('test'),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        shape: CircularNotchedRectangle(),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: FormBuilderDropdown<String>(
            // autovalidate: true,
            name: 'area',
            decoration: const InputDecoration(
              labelText: '지역',
              hintText: '지역 선택',
              labelStyle: TextStyle(color: Colors.white),
            ),
            items: areaOptions
                .map((area) => DropdownMenuItem(
                      alignment: AlignmentDirectional.center,
                      value: area,
                      child: Text(area),
                    ))
                .toList(),
            onChanged: (val) {
              setState(() {
                _searchArea = _formKey.currentState!.fields['area']!.value;
                _loadItems(refresh: true);
              });
            },
            valueTransformer: (val) => val?.toString(),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    if (_matchCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(icon: Icons.local_hospital, count: _matchCount, unitName: '건', itemName: '병원'),
          const SizedBox(height: 20,),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentListItem(item: _list[index], callback: () {}),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 50, // appBar의 크기가 대략 50dp
        child: const ComponentNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다.'),
      );
    }
  }
}
