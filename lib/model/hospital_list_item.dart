class HospitalListItem {
  String? orgcd;
  String? orgnm;
  String? orgTlno;
  String? orgZipaddr;
  String? slrYmd;
  String? dywk;
  String? hldyYn;
  String? lunchSttTm;
  String? lunchEndTm;
  String? sttTm;
  String? endTm;

  HospitalListItem({
    this.orgcd,
    this.orgnm,
    this.orgTlno,
    this.orgZipaddr,
    this.slrYmd,
    this.dywk,
    this.hldyYn,
    this.lunchSttTm,
    this.lunchEndTm,
    this.sttTm,
    this.endTm
  });

  factory HospitalListItem.fromJson(Map<String, dynamic> json) {
    return HospitalListItem(
      orgcd: json['orgcd'] != null ? json['orgcd'] : null,
      orgnm: json['orgnm'] != null ? json['orgnm'] : null,
      orgTlno: json['orgTlno'] != null ? json['orgTlno'] : null,
      orgZipaddr: json['orgZipaddr'] != null ? json['orgZipaddr'] : null,
      slrYmd: json['slrYmd'] != null ? json['slrYmd'] : null,
      dywk: json['dywk'] != null ? json['dywk'] : null,
      hldyYn: json['hldyYn'] != null ? json['hldyYn'] : null,
      lunchSttTm: json['lunchSttTm'] != null ? json['lunchSttTm'] : null,
      lunchEndTm: json['lunchEndTm'] != null ? json['lunchEndTm'] : null,
      sttTm: json['sttTm'] != null ? json['sttTm'] : null,
      endTm: json['endTm'] != null ? json['endTm'] : null,
    );
  }
}