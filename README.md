# 코로나 백신 예약 가능 병원 조회 APP
***
(개인 프로젝트) 코로나 백신 예약가능한 병원을 조회하는 APP / 공공데이터 OPEN API 사용
***

### VERSION
```
0.0.1
```

### LANGUAGE
```
Dart 2.18.1
Flutter 3.3.2
```

### DURATION
```
총 제작일수 : 2일
```

### 기능
```
* 병원 조회
```

### 실제 앱 화면
> * 병원 조회
>
>![hospital_main](./images/main.PNG)
